#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

enum class MovementDirection
{
	Up,
	Down,
	Right,
	Left,
};

namespace gameplay
{
	const int maxObjetives = 20;
	const int maxLevel = 5;
	const Color texColor = BLACK;

	extern bool gameInitialized;
	
	void draw();
	void play();
	void deinit();
}

#endif // !GAMEPLAY_H


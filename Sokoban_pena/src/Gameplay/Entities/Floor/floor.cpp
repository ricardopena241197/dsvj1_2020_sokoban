#include "floor.h"
#include <iostream>
#include <string.h>

namespace floors
{
	void loadLevel(int levelNumber, Floor level[gridSize][gridSize] )
	{
		using namespace std;
		string path= "\0";
		int index = 0;

		switch (levelNumber)
		{
		case 1:					
			path = "res/levels/level1.txt";
			break;
		case 2:
			path = "res/levels/level2.txt";
			break;
		case 3:
			path = "res/levels/level3.txt";
			break;
		case 4:
			path = "res/levels/level4.txt";
			break;
		case 5:
			path =  "res/levels/level5.txt";
			break;

		default:
			
			break;
		}
		char auxPath[100] = "\0";
		strcpy_s(auxPath, path.c_str());

		char* uploadLevel = { LoadFileText(auxPath) };

		string stringLevelUploaded(uploadLevel);

		for (int i = 0; i < gridSize; i++)
		{
			for (int j = 0; j < gridSize; j++)
			{
				switch (stringLevelUploaded[index])
				{
				case '0':
					level[i][j].floorType = FloorType::Inactive;
					break;
				case '1':
					level[i][j].floorType = FloorType::Active;
					break;
				case '2':
					level[i][j].floorType = FloorType::Wall;
					break;
				case '3':
					level[i][j].floorType = FloorType::X;
					break;
				case '4':
					level[i][j].floorType = FloorType::Character;
					break;
				case '5':
					level[i][j].floorType = FloorType::Box;
					break;
				default:
					break;
				}
				index++;
			}
			index++;
		}
	}
}
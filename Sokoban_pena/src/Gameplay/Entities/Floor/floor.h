#ifndef FLOOR_H
#define FLOOR_H

#include "raylib.h"

enum class FloorType
{
	Inactive,
	Active,
	Wall,
	X,
	Character,
	Box
};

struct Floor
{
	FloorType floorType;
	Rectangle collider;
	Color dye;
	int amountOfBoxes;
};

const int gridSize = 20;

namespace floors
{
	void loadLevel(int levelNumber, Floor level[gridSize][gridSize]);
}

#endif // !FLOOR_H


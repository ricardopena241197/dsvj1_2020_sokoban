#include "gameplay.h"
#include "Game/game.h"
#include "Window/window.h"
#include "Textures/textures.h"
#include "Gameplay/Entities/Floor/floor.h"
#include "Text/text.h"
#include "Sounds/sounds.h"
#include "Controls/controls.h"

#include <iostream>
#include <stdlib.h>

using namespace std;

namespace gameplay
{
	bool gameInitialized = false;

	Texture2D background;
	Vector2 backgroundPos;
	Color backgoundDye;	
	
	Texture2D character;
	Texture2D box;
	Texture2D floor;
	Texture2D x;

	Vector2 characterPos;
	Vector2 objetivePos[maxObjetives];
	int amountOfObjetives;

	int levelNumber;

	bool gameOver;	

	Floor level[gridSize][gridSize];

	namespace initialize
	{
		static void initBackground()
		{
			using namespace textures;

			background = LoadTexture(backgroundTexturePath);
			background.height = window::screen.height;
			background.width = window::screen.width;

			backgroundPos.x = 0;
			backgroundPos.y = 0;

			backgoundDye = WHITE;
		}	

		static void initLevel()
		{
			floors::loadLevel(levelNumber, level);

			int auxX = (((window::screen.width / 2) / gridSize)*gridSize)/2;
			int auxY = (window::screen.height /2) - (((window::screen.width / 2) / gridSize) * gridSize) / 2;

			for (short i = 0; i < gridSize; i++)
			{
				for (short j = 0; j < gridSize; j++)
				{
					level[i][j].dye = WHITE;
					level[i][j].collider.width = (window::screen.width / 2) / gridSize;
					level[i][j].collider.height = (window::screen.width / 2) / gridSize;
					level[i][j].collider.x = auxX;
					level[i][j].collider.y = auxY;
					auxX += (window::screen.width / 2) / gridSize;
				}
				auxX = (((window::screen.width / 2) / gridSize) * gridSize) / 2;
				auxY += (window::screen.width / 2) / gridSize;
			}

			character = LoadTexture(textures::characterTexturePath);
			character.width = level[0][0].collider.width;
			character.height = level[0][0].collider.width;

			box = LoadTexture(textures::boxTexturePath);
			box.width = level[0][0].collider.width;
			box.height = level[0][0].collider.width;

			floor = LoadTexture(textures::floorTexturePath);
			floor.width = level[0][0].collider.width;
			floor.height = level[0][0].collider.width;

			x = LoadTexture(textures::xTexturePath);
			x.width = level[0][0].collider.width;
			x.height = level[0][0].collider.width;

		}

		static void setLevel(Vector2& characterPos, Vector2 objetivePos[maxObjetives],int& amountOfObjetives)
		{
			amountOfObjetives = 0;
			floors::loadLevel(levelNumber, level);
			for (short i = 0; i < gridSize; i++)
			{
				for (short j = 0; j < gridSize; j++)
				{
					if (level[i][j].floorType==FloorType::Character)
					{
						characterPos.x = j;
						characterPos.y = i;
					}

					if (level[i][j].floorType == FloorType::X)
					{
						objetivePos[amountOfObjetives].x = j;
						objetivePos[amountOfObjetives].y = i;
						amountOfObjetives++;
					}
				}
			}
		}
	}

	namespace updating
	{
		static void setEmpyObjetives()
		{
			for (short i = 0; i < amountOfObjetives; i++)
			{
				if (level[int(objetivePos[i].y)][int(objetivePos[i].x)].floorType == FloorType::Active)
				{
					level[int(objetivePos[i].y)][int(objetivePos[i].x)].floorType = FloorType::X;
				}
			}
		}

		static bool checkLevelPass()
		{
			for (short i = 0; i < amountOfObjetives; i++)
			{
				if (level[int(objetivePos[i].y)][int(objetivePos[i].x)].floorType != FloorType::Box)
				{
					return false;
				}

			}
			return true;
		}

		static void levelPass()
		{
			if (checkLevelPass())
			{
				levelNumber++;
				if (levelNumber == maxLevel)
				{
					gameOver = true;
				}
				else
				{					
					initialize::setLevel(characterPos, objetivePos, amountOfObjetives);
				}
			}
		}

		static bool checkNextPosition(Vector2 nextPos)
		{
			int i = nextPos.y;
			int j = nextPos.x;
			return (level[i][j].floorType == FloorType::Active || level[i][j].floorType == FloorType::X);
		}

		static bool checkBoxNextPosition(Vector2 nextPos)
		{			
			return (level[int(nextPos.y)][int(nextPos.x)].floorType==FloorType::Box);
		}

		static void boxMovement(MovementDirection movementDirection, Vector2& boxPos, Vector2 characterPrevPos)
		{
			Vector2 nextPos = { 0,0 };
			switch (movementDirection)
			{
			case MovementDirection::Up:
				if (boxPos.y > 0)
				{
					nextPos = { boxPos.x,boxPos.y - 1 };
				}

				break;
			case MovementDirection::Down:
				if (boxPos.y < gridSize - 1)
				{
					nextPos = { boxPos.x,boxPos.y + 1 };
				}

				break;
			case MovementDirection::Right:
				if (boxPos.x < gridSize - 1)
				{
					nextPos = { boxPos.x + 1 ,boxPos.y };
				}

				break;
			case MovementDirection::Left:
				if (boxPos.y > 0)
				{
					nextPos = { boxPos.x - 1,boxPos.y };
				}
				break;
			default:
				nextPos = boxPos;
				break;
			}

			if (checkNextPosition(nextPos))
			{
				level[int(characterPrevPos.y)][int(characterPrevPos.x)].floorType = FloorType::Active;
				level[int(boxPos.y)][int(boxPos.x)].floorType = FloorType::Character;
				level[int(nextPos.y)][int(nextPos.x)].floorType = FloorType::Box;
				characterPos = boxPos;
			}
		}

		static void movement(MovementDirection movementDirection, Vector2& characterPos)
		{
			Vector2 nextPos = {0,0};
			switch (movementDirection)
			{
			case MovementDirection::Up:
				if (characterPos.y > 0)
				{
					nextPos = { characterPos.x,characterPos.y - 1 };
				}
				
				break;
			case MovementDirection::Down:
				if (characterPos.y < gridSize-1)
				{
					nextPos = { characterPos.x,characterPos.y + 1 };
				}
				
				break;
			case MovementDirection::Right:
				if (characterPos.x < gridSize - 1)
				{
					nextPos = { characterPos.x + 1 ,characterPos.y };
				}
				
				break;
			case MovementDirection::Left:
				if (characterPos.y > 0)
				{
					nextPos = { characterPos.x - 1,characterPos.y };
				}				
				break;
			default:
				nextPos = characterPos;
				break;
			}

			if (checkNextPosition(nextPos))
			{
				level[int(characterPos.y)][int(characterPos.x)].floorType = FloorType::Active;
				level[int(nextPos.y)][int(nextPos.x)].floorType = FloorType::Character;
				characterPos = nextPos;
			}
			else
			{
				if (checkBoxNextPosition(nextPos))
				{
					boxMovement(movementDirection, nextPos, characterPos);
				}
			}

			setEmpyObjetives();

		}

		static void imput()
		{
			using namespace controls;

			if (IsKeyPressed(keyPause))
			{
				game::actualScene = Scenes::Pause;
			}

			if (IsKeyPressed(keyUp))
			{
				movement(MovementDirection::Up, characterPos);
			}

			if (IsKeyPressed(keyDown))
			{
				movement(MovementDirection::Down, characterPos);
			}

			if (IsKeyPressed(keyLeft))
			{
				movement(MovementDirection::Left, characterPos);
			}

			if (IsKeyPressed(keyRight))
			{
				movement(MovementDirection::Right, characterPos);
			}


		}
	}

	namespace drawing
	{
		static void drawBackground()
		{
			DrawTexture(background,backgroundPos.x,backgroundPos.y,backgoundDye);
		}	

		static void drawLevel()
		{
			for (short i = 0; i < gridSize; i++)
			{
				for (short j = 0; j < gridSize; j++)
				{
					switch (level[i][j].floorType)
					{
					case FloorType::Inactive:
						DrawRectangleRec(level[i][j].collider,BLACK);
						break;
					case FloorType::Active:
						DrawTexture(floor, level[i][j].collider.x, level[i][j].collider.y, level[i][j].dye);
						break;
					case FloorType::Wall:
						DrawRectangleRec(level[i][j].collider, GRAY);
						break;
					case FloorType::X:
						DrawTexture(x, level[i][j].collider.x, level[i][j].collider.y, level[i][j].dye);
						break;
					case FloorType::Character:
						DrawTexture(character, (level[i][j].collider.x+ level[i][j].collider.width/2) - character.width/2, level[i][j].collider.y, level[i][j].dye);
						break;
					case FloorType::Box:
						DrawTexture(box, level[i][j].collider.x, level[i][j].collider.y, level[i][j].dye);
						break;
					default:
						break;
					}

				}
			}
		}
	}

	namespace deinitialize
	{
		static void deinitBackground()
		{
			UnloadTexture(background);			
		}		
	}

	static void init()
	{
		using namespace initialize;
		initBackground();
		levelNumber = 1;
		initLevel();
		setLevel(characterPos,objetivePos,amountOfObjetives);
		gameOver = false;
		
	}

	static void update()
	{
		using namespace updating;		
		imput();
		levelPass();
		if (gameOver)
		{
			game::actualScene = Scenes::MainMenu;
		}
	}

	void draw()
	{
		using namespace drawing;
		drawBackground();
		drawLevel();
		
	}

	void play()
	{
		if (!gameInitialized)
		{
			init();
			gameInitialized = true;
		}

		update();
	}

	void deinit()
	{
		using namespace deinitialize;

		gameInitialized = false;
		deinitBackground();		
		
	}
}
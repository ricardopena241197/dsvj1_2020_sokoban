#include "sounds.h"

using namespace std;

namespace sounds
{
	Music mainMenu;
	Music gamePlay;
	bool musicActive;

	Sound movingMenu;
	bool playMovingMenu;

	Sound acceptMenu;
	bool playAccepMenu;

	bool soundsActive;

	float musicVolume;
	float soundVolume;	

	void initSounds()
	{
		mainMenu = LoadMusicStream("res/assets/music/SpringVillage.OGG");
		gamePlay = LoadMusicStream("res/assets/music/Battleship.OGG");
		musicActive = true;

		movingMenu = LoadSound("res/assets/sounds/move.wav");
		playMovingMenu = false;

		acceptMenu = LoadSound("res/assets/sounds/accept.wav");
		playAccepMenu = false;

		soundsActive = true;
		musicVolume	= defaulVolume;
		soundVolume	= defaulVolume;

		SetMusicVolume(mainMenu, musicVolume);
		SetMusicVolume(gamePlay, musicVolume);

		SetSoundVolume(movingMenu, soundVolume);
		SetSoundVolume(acceptMenu, soundVolume);
	}	

	void deinitSounds()
	{
		UnloadMusicStream(mainMenu);
		UnloadMusicStream(gamePlay);		
	}

	void playSounds()
	{
		if (playMovingMenu)
		{
			playMovingMenu = false;
			PlaySound(movingMenu);
		}

		if (playAccepMenu)
		{
			playAccepMenu = false;
			PlaySound(acceptMenu);
		}
	}

	void muteUnmuteMusic()
	{
		musicActive = !musicActive;
		if (musicActive)
		{
			SetMusicVolume(mainMenu, musicVolume);
			SetMusicVolume(gamePlay, musicVolume);
		}
		else
		{
			SetMusicVolume(mainMenu, 0);
			SetMusicVolume(gamePlay, 0);
		}
	}

	void muteUnmuteEfects()
	{
		soundsActive = !soundsActive;
		if (soundsActive)
		{
			SetSoundVolume(movingMenu, soundVolume);
			SetSoundVolume(acceptMenu, soundVolume);
		}
		else
		{
			SetSoundVolume(movingMenu, 0);
			SetSoundVolume(acceptMenu, 0);
		}
	}

	void levelUpMusicVolume()
	{
		if (musicVolume < 1.0f)
		{
			musicVolume = musicVolume + 0.1f;
			SetMusicVolume(mainMenu, musicVolume);
			SetMusicVolume(gamePlay, musicVolume);
			
		}		
	}

	void levelDownMusicVolume()
	{
		if (musicVolume > 0.1f)
		{
			musicVolume = musicVolume - 0.1f;
			SetMusicVolume(mainMenu, musicVolume);
			SetMusicVolume(gamePlay, musicVolume);
			
		}
		else
		{
			musicVolume = 0;
			SetMusicVolume(mainMenu, musicVolume);
			SetMusicVolume(gamePlay, musicVolume);
		}
	}

	void levelUpSoundVolume()
	{
		if (soundVolume < 1.0f)
		{
			soundVolume = soundVolume + 0.1f;
			SetSoundVolume(movingMenu, soundVolume);
			SetSoundVolume(acceptMenu, soundVolume);
		}
	}

	void levelDownSoundVolume()
	{
		if (soundVolume > 0.1f)
		{
			soundVolume = soundVolume - 0.1f;
			SetSoundVolume(movingMenu, soundVolume);
			SetSoundVolume(acceptMenu, soundVolume);
		}
		else
		{
			soundVolume = 0;
			SetSoundVolume(movingMenu, soundVolume);
			SetSoundVolume(acceptMenu, soundVolume);
		}
	}
}
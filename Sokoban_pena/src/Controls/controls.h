#ifndef CONTROLS_H
#define CONTROLS_H

namespace controls
{
	extern int keyUp;
	extern int keyDown;
	extern int keyRight;
	extern int keyLeft;
	extern int keyPause;
	extern int keyEnter;

	void init();
}

#endif // !CONTROLS_H


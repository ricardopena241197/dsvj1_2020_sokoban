#include "controls.h"
#include "raylib.h"

namespace controls
{
	int keyUp;
	int keyDown;
	int keyRight;
	int keyLeft;
	int keyPause;
	int keyEnter;

	void init()
	{
		//Default Controls
		keyUp=KEY_UP;
		keyDown = KEY_DOWN;
		keyRight = KEY_RIGHT;
		keyLeft = KEY_LEFT;
		keyPause = KEY_P;
		keyEnter = KEY_ENTER;
	}
}

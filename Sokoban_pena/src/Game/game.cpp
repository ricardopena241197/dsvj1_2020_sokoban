#include "game.h"
#include "raylib.h"
#include "Global Variables/global_variables.h"
#include "Window/window.h"
#include "Controls/controls.h"
#include "Sounds/sounds.h"
#include "Menus/Main Menu/main_menu.h"
#include "Menus/Settings Menu/settings_menu.h"
#include "Menus/Settings Menu/Music Menu/music_menu.h"
#include "Menus/Settings Menu/Sounds Menu/sounds_menu.h"
#include "Menus/Settings Menu/Graphics Menu/graphics_menu.h"
#include "Menus/Credits Menu/credits.h"
#include "Menus/Pause Menu/pause_menu.h"
#include "Gameplay/gameplay.h"

#include <iostream>
#include <time.h>


namespace game
{
	Scenes actualScene;

	static void init()
	{
		global_variables::auxExit = false;
		actualScene = Scenes::MainMenu;
		window::init();
		controls::init();
		InitAudioDevice();
		sounds::initSounds();
		srand(time(NULL));
		HideCursor();
		DisableCursor();
	}

	static void update()
	{
		if (actualScene!=Scenes::Play)
		{
			PlayMusicStream(sounds::mainMenu);
			UpdateMusicStream(sounds::mainMenu);
		}
		else
		{
			PlayMusicStream(sounds::gamePlay);
			UpdateMusicStream(sounds::gamePlay);
		}

		sounds::playSounds();
		switch (actualScene)
		{
		case Scenes::MainMenu:
			main_menu::menu();
			break;
		case Scenes::Play:
			gameplay::play();
			break;
		case Scenes::Settings:
			settings_menu::menu();
			break;
		case Scenes::MusicMenu:
			music_menu::menu();
			break;
		case Scenes::SoundsMenu:
			sounds_menu::menu();
			break;
		case Scenes::GraphicsMenu:
			graphics_menu::menu();
			break;
		case Scenes::Credits:
			credits::menu();
			break;
		case Scenes::Pause:
			pause_menu::menu();
			break;
		default:
			break;
		}
	}

	static void draw()
	{
		BeginDrawing();
		ClearBackground(RAYWHITE);
		switch (actualScene)
		{
		case Scenes::MainMenu:
			main_menu::draw();
			break;
		case Scenes::Play:
			gameplay::draw();
			break;
		case Scenes::Settings:
			settings_menu::draw();
			break;
		case Scenes::MusicMenu:
			music_menu::draw();
			break;
		case Scenes::SoundsMenu:
			sounds_menu::draw();
			break;
		case Scenes::GraphicsMenu:
			graphics_menu::draw();
			break;
		case Scenes::Credits:
			credits::draw();
			break;
		case Scenes::Pause:
			pause_menu::draw();
			break;
		default:
			break;
		}
		EndDrawing();
	}

	static void deinit()
	{
		CloseAudioDevice();
		sounds::deinitSounds();
		gameplay::deinit();
		
	}

	void game()
	{
		init();

		while (!WindowShouldClose() && !global_variables::auxExit)
		{			
			draw();
			update();
		}	
		deinit();
	}
}
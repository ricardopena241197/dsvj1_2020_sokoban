#ifndef GAME_H
#define GAME_H

enum class Scenes
{
	MainMenu,
	Play,
	Settings,
	MusicMenu,
	SoundsMenu,
	GraphicsMenu,
	Credits,
	Pause
};

namespace game
{
	extern Scenes actualScene;

	void game();
}

#endif // !GAME_H


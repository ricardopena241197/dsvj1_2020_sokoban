#include "text.h"
#include "raylib.h"

namespace text
{
	int measureStringText(string text, int textSize)
	{
		
		using namespace main_menu;
		char auxText[maximumAmountOfCharacters];

		strcpy_s(auxText, text.c_str());

		return MeasureText(auxText, textSize);
		
	}
}
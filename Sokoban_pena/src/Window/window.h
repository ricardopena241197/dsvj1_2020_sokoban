#ifndef WINDOW_H
#define WINDOW_H

enum class ScreenQuality
{
	SD,
	qHD,
	HD,
	FHD
};

struct Screen
{
	int height;
	int width;
	ScreenQuality quality;		
};

namespace window
{
	const char title[] = "Fairies Puzzles";	
	extern Screen screen;
	void init();
	void changeResolution(ScreenQuality desiredQuality);
}

#endif // !WINDOW_H


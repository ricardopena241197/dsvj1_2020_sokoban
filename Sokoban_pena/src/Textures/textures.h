#ifndef TEXTURES_H
#define TEXTURES_H

#include "Menus/Main Menu/main_menu.h"
#include "raylib.h"

namespace textures
{
	//File path for texture assets

	//Gameplay
		//Stage	
	const char backgroundTexturePath[] = "res/assets/textures/Gameplay/background.png";	
	const char characterTexturePath[] = "res/assets/textures/Gameplay/character.png";
	const char boxTexturePath[] = "res/assets/textures/Gameplay/box.png";
	const char floorTexturePath[] = "res/assets/textures/Gameplay/floor.png";
	const char xTexturePath[] = "res/assets/textures/Gameplay/x.png";

	//Menus
	const char optionsTexturePath[] = "res/assets/textures/menus/options_texture.png";
	const char menuBackgroundTexturePath[] = "res/assets/textures/menus/background.png";

	//Others

	extern int	buttonBorderHeight;
	void initTitleTexture(Texture2D& texture, MenuOptions option);
	void initOptionsTexture(Texture2D& texture, MenuOptions option);
}

#endif // !TEXTURES_H


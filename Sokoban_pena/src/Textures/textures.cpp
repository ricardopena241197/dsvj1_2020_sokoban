#include "textures.h"


namespace textures
{
	int	buttonBorderHeight;
	void initTitleTexture(Texture2D& texture, MenuOptions option)
	{
		texture.height = option.textSize+ option.textSize/6;
		texture.width = option.size + option.size/5;
	}

	void initOptionsTexture(Texture2D& texture, MenuOptions option)
	{
		buttonBorderHeight = window::screen.height / 18;
		texture.height = option.textSize + buttonBorderHeight / 2;
		texture.width = option.size + buttonBorderHeight;
	}
}